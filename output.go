package main

import (
	"context"
	"os"
	"path/filepath"
	"strings"

	"github.com/mholt/archiver/v4"
	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/recipe"
	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/util"
)

func prepOutput(output, desc string) {
	root := filepath.Dir(output)
	if !util.IsRealDir(root) {
		util.Exit(root, "is not a valid dir")
	}

	if util.IsRealDatapack(output) {
		util.Exit(output, "already exists")
	}

	util.MakeDir(filepath.Join(output, "data"))

	path := filepath.Join(output, "pack.mcmeta")
	util.MakeJsonFile(path, util.JSObj{
		"pack": util.JSObj{
			"pack_format": 26,
			"description": desc,
		},
	})
}

func writeRecipes(output string, recipes []recipe.Recipe) {
	for _, recipe := range recipes {
		path := filepath.Join(output, recipe.Parent, "recipes")
		util.MakeDir(path)

		path = filepath.Join(path, recipe.Name)
		util.MakeJsonFile(path, recipe.JSObj)
	}
}

func writeAdvancements(output string, recipes []recipe.Recipe) {
	for _, recipe := range recipes {
		path := filepath.Join(output, recipe.Parent, "advancements", "recipes")
		util.MakeDir(path)
		path = filepath.Join(path, recipe.Name)

		data := util.JSObj{
			"parent": "minecraft:recipes/root",
			"criteria": util.JSObj{
				"has_item": util.JSObj{
					"trigger": "minecraft:inventory_changed",
					"conditions": util.JSObj{
						"items": getAdvancementItems(recipe),
					},
				},
			},
			"requirements": util.JSList{
				util.JSList{"has_item"},
			},
			"rewards": util.JSObj{
				"recipes": util.JSList{recipe.FullName()},
			},
		}

		util.MakeJsonFile(path, data)
	}
}

func getAdvancementItems(r recipe.Recipe) util.JSList {
	items := util.JSList{}
	for _, ingr := range r.Ingredients {
		toAdd := util.JSObj{}
		if tag, ok := ingr.(recipe.Tag); ok {
			toAdd["tag"] = tag
		} else {
			toAdd["items"] = util.JSList{
				ingr.(recipe.Item),
			}
		}

		items = append(items, toAdd)
	}

	return items
}

func createZip(output string) {
	root := filepath.Dir(output)
	pack := filepath.Join(root, "pack.mcmeta")
	zip := strings.TrimSuffix(root, "/") + ".zip"

	files, err := archiver.FilesFromDisk(nil, map[string]string{
		output: "",
		pack:   "",
	})
	util.MustBeNil(err)

	out, err := os.Create(zip)
	util.MustBeNil(err)
	defer out.Close()

	format := archiver.CompressedArchive{
		Archival: archiver.Zip{},
	}

	err = format.Archive(context.Background(), out, files)
	util.MustBeNil(err)
}
