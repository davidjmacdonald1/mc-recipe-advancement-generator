package main

import (
	"os"
	"path/filepath"
	"strings"
	"unicode"

	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/util"
)

func handleArgs() (parent, output string, datapacks []string) {
	if len(os.Args) < 5 {
		util.Exit("exe NAME DESC OUT DATAPACKS...")
	}

	name := parseName(os.Args[1])
	desc := os.Args[2]
	out := os.Args[3]
	datapacks = os.Args[4:]

	parent = strings.ReplaceAll(name, "-", "_")
	output = filepath.Join(out, name)
	prepOutput(output, desc)
	output = filepath.Join(output, "data")

	return parent, output, datapacks
}

func parseName(name string) string {
	if strings.ContainsFunc(name, isInvalidNameRune) {
		util.Exit("Name", name, `should only contain letters and "-_ "`)
	}

	return strings.ReplaceAll(name, " ", "-")
}

func isInvalidNameRune(r rune) bool {
	isLetter := unicode.IsLetter(r)
	isSym := strings.ContainsRune("-_ ", r)
	return !isLetter && !isSym
}
