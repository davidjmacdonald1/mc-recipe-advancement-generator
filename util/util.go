package util

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

type JSObj = map[string]any
type JSList = []any

func Exit(a ...any) {
	fmt.Println(a...)
	os.Exit(1)
}

func Exitf(format string, a ...any) {
	fmt.Printf(format, a...)
	os.Exit(1)
}

func MustBeNil(err error, show ...any) {
	if err == nil {
		return
	}

	if len(show) == 0 {
		show = []any{err.Error()}
	}

	Exit(show...)
}

func IsRealPath(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func IsRealFile(path string) bool {
	stat, err := os.Stat(path)
	return err == nil && !stat.IsDir()
}

func IsRealJsonFile(path string) bool {
	return strings.HasSuffix(path, ".json") && IsRealFile(path)
}

func IsRealDir(path string) bool {
	stat, err := os.Stat(path)
	return err == nil && stat.IsDir()
}

func IsRealDatapack(path string) bool {
	hasData := IsRealDir(filepath.Join(path, "data"))
	hasPack := IsRealFile(filepath.Join(path, "pack.mcmeta"))
	return hasData && hasPack
}

func MakeDir(path string) {
	err := os.MkdirAll(path, 0644)
	MustBeNil(err)
}

func MakeJsonFile(path string, v any) {
	data, err := json.MarshalIndent(v, "", "    ")
	MustBeNil(err)
	err = os.WriteFile(path, data, 0644)
	MustBeNil(err)
}
