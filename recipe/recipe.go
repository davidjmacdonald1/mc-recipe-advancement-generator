package recipe

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/util"
)

type Recipe struct {
	Parent      string
	Name        string
	Type        string
	JSObj       util.JSObj
	Ingredients []Ingredient
	Result      Item
}

func (r Recipe) FullName() string {
	name := strings.TrimSuffix(r.Name, ".json")
	return fmt.Sprint(r.Parent, ":", name)
}

func (r *Recipe) generateName(name string) {
	if r.Parent == "minecraft" {
		r.Name = name
		return
	}

	result := strings.Split(string(r.Result), ":")[1]
	r.Name = fmt.Sprint(r.Type, "_", result)

	if len(r.Ingredients) == 1 {
		ingredient := r.Ingredients[0].Ingredient()
		input := strings.Split(ingredient, ":")[1]
		r.Name += fmt.Sprint("_from_", input)
	}

	r.Name += ".json"
}

func ReadDatapack(datapack string) []Recipe {
	dirs, err := os.ReadDir(filepath.Join(datapack, "data"))
	util.MustBeNil(err)

	recipes := []Recipe{}
	for _, parent := range dirs {
		path := filepath.Join(datapack, "data", parent.Name(), "recipes")
		if !util.IsRealPath(path) {
			continue
		}

		found := readAllRecipes(path, parent.Name())
		recipes = append(recipes, found...)
	}

	return recipes
}

func readAllRecipes(recipesPath, parent string) []Recipe {
	files, err := os.ReadDir(recipesPath)
	util.MustBeNil(err)

	recipes := make([]Recipe, 0, len(files))
	for _, file := range files {
		name := file.Name()
		path := filepath.Join(recipesPath, name)
		if !util.IsRealJsonFile(path) {
			continue
		}

		recipe := readRecipe(path)
		recipe.Parent = parent

		if len(recipe.Ingredients) == 0 {
			fmt.Println("Recipe", recipe.Name, "is formatted wrong")
		} else {
			recipe.generateName(name)
			recipes = append(recipes, recipe)
		}
	}

	return recipes
}

func readRecipe(path string) Recipe {
	data, err := os.ReadFile(path)
	util.MustBeNil(err)

	jsobj := util.JSObj{}
	err = json.Unmarshal(data, &jsobj)
	util.MustBeNil(err)

	ingredients := getIngredients(jsobj)
	Type := getType(jsobj)
	result := getResult(jsobj)

	if Type == "" || result == "" {
		ingredients = []Ingredient{}
	}

	return Recipe{
		Type:        Type,
		JSObj:       jsobj,
		Ingredients: ingredients,
		Result:      result,
	}
}

func getType(recipe util.JSObj) string {
	if t, ok := recipe["type"].(string); ok {
		t = strings.Split(t, ":")[1]
		if strings.Contains(t, "crafting") {
			t = "crafting"
		}

		return t
	}

	return ""
}
