package recipe

import (
	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/util"
)

type Ingredient interface {
	Ingredient() string
}

type Tag string

func (t Tag) Ingredient() string {
	return string(t)
}

type Item string

func (i Item) Ingredient() string {
	return string(i)
}

func getIngredients(recipe util.JSObj) []Ingredient {
	results := appendIngr([]Ingredient{}, recipe["ingredient"])

	if key, ok := recipe["key"].(util.JSObj); ok {
		for _, value := range key {
			results = appendIngr(results, value)
		}
	}

	if ingrs, ok := recipe["ingredients"].(util.JSList); ok {
		for _, value := range ingrs {
			results = appendIngr(results, value)
		}
	}

	return results
}

func getTagOrItem(ingr util.JSObj) Ingredient {
	if tag, ok := ingr["tag"].(string); ok {
		return Tag(tag)
	}

	if item, ok := ingr["item"].(string); ok {
		return Item(item)
	}

	return nil
}

func appendIngr(slice []Ingredient, value any) []Ingredient {
	if ingr, ok := value.(util.JSObj); ok {
		result := getTagOrItem(ingr)
		if result != nil {
			return append(slice, result)
		}
	}

	return slice
}
