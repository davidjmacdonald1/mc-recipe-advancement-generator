package recipe

import "gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/util"

func getResult(recipe util.JSObj) Item {
	if item, ok := recipe["result"].(string); ok {
		return Item(item)
	}

	if itemMap, ok := recipe["result"].(util.JSObj); ok {
		if item, ok := itemMap["item"].(string); ok {
			return Item(item)
		}
	}

	return ""
}
