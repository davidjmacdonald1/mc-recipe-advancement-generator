package main

import (
	"fmt"
	"strings"

	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/recipe"
	"gitlab.com/davidjmacdonald1/mc-recipe-advancement-generator/util"
)

func main() {
	parent, output, datapacks := handleArgs()
	recipes := []recipe.Recipe{}
	for _, datapack := range datapacks {
		if !util.IsRealDatapack(datapack) {
			fmt.Println(datapack, "is not a valid datapack")
			continue
		}

		found := recipe.ReadDatapack(datapack)
		recipes = append(recipes, found...)
	}

	resolveNameConflicts(parent, recipes)
	writeRecipes(output, recipes)
	writeAdvancements(output, recipes)
	createZip(output)
}

func resolveNameConflicts(parent string, recipes []recipe.Recipe) {
	names := map[string]int{}
	for i, recipe := range recipes {
		if recipe.Parent != "minecraft" {
			recipes[i].Parent = parent
		}

		name := recipe.Name
		if _, ok := names[name]; !ok {
			names[name] = 1
			continue
		}

		names[name] += 1
		prefix := strings.TrimSuffix(name, ".json")
		recipes[i].Name = fmt.Sprint(prefix, names[name], ".json")
	}
}
